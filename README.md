Messaging
=========

Messaging Middleware zum Auto Shop.

Optional nach Installation:

    microk8s enable metallb:192.168.205.250-192.168.205.254
    
Functions
---------

* [Doku](https://knative.dev/docs/functions/)    

Serving
-------

* Installation siehe [Infrastrukur Kubernetes](https://gitlab.com/ch-mc-b/autoshop-ms/infra/kubernetes-templates#installation).

Überprüfen ob der Ingress Service aktiv ist:

    kubectl --namespace kourier-system get service kourier
    PORT=$(kubectl get service --namespace kourier-system kourier -o=jsonpath='{ .spec.ports[0].nodePort }')

Customer und Catalog als Serverless Services

    kn service create catalog --image registry.gitlab.com/ch-mc-b/autoshop-ms/app/shop/catalog:2.1.0 --port 8080
    curl -H "Host: catalog.default.microk8s.mshome.net" localhost:${PORT}/catalog/api     
    
    kn service create customer --image registry.gitlab.com/ch-mc-b/autoshop-ms/app/shop/customer:2.1.0 --port 8080 
    curl -H "Host: customer.default.microk8s.mshome.net" localhost:${PORT}/customer/api
    
Eventing
--------

### Source to Sink (Fire and Forget)

Source to Service bietet den einfachsten Einstieg in Knative Eventing. Es bietet einen einzigen Sink  – also einen Event-Empfangsdienst – ohne Warteschlangen, Gegendruck und Filterung. Source to Service unterstützt keine Antworten, was bedeutet, dass die Antwort vom Sink- Dienst ignoriert wird.

    kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.14.2/eventing-crds.yaml
    kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.14.2/eventing-core.yaml
    
    kubectl get pods -n knative-eventing    
    
**Testen (siehe RedHat Tutorial)**

Service erstellen welche auf den Event horcht

    kn service create eventinghello \
      --concurrency-target=1 \
      --image=quay.io/rhdevelopers/eventinghello:0.0.2

Event auslösen

    PORT=$(kubectl get service --namespace kourier-system kourier -o=jsonpath='{ .spec.ports[0].nodePort }')
    curl -X POST -H "Host: eventinghello.default.microk8s.mshome.net" -H "Content-Type: application/json" \
    -H "ce-id: ich" -d '{"message": "Hallo, ich bins!"}' http://localhost:${PORT}    

### Channel

In Memory Channel Broker erstellen

    kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.14.2/in-memory-channel.yaml

Kanäle
* Kanäle sind eine Ereignisweiterleitungs- und Persistenzschicht, wobei jeder Kanal eine separate benutzerdefinierte Kubernetes-Ressource ist. Ein Kanal kann von Apache Kafka oder InMemoryChannel unterstützt werden. Dieses Rezept konzentriert sich auf InMemoryChannel.

Abonnements
* Mit Abonnements registrieren Sie Ihren Dienst, um einen bestimmten Kanal zu hören.

Channel erstellen
    
    kn channel create eventinghello-ch
    kn channel ls
    


    
### Links

* [Knative](https://knative.dev/docs/)
* [Cloudevents - A specification for describing event data in a common way](https://cloudevents.io/)
* [RedHat Tutorial](https://redhat-developer-demos.github.io/knative-tutorial/knative-tutorial/serving/index.html)
* [Code Samples](https://github.com/knative/docs/tree/main/code-samples)
* [Eventing Samples](https://knative.dev/docs/samples/eventing/)
* [Quickstart](https://knative.dev/docs/getting-started/)
* [Kafka Sink](https://knative.dev/docs/eventing/sinks/kafka-sink/)
* [sslip](https://github.com/cunnie/sslip.io/tree/main)

* [Digital Ocean](https://docs.digitalocean.com/products/marketplace/catalog/knative/)    